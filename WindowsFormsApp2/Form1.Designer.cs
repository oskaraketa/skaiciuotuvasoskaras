﻿namespace WindowsFormsApp2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.Pliusas = new System.Windows.Forms.Button();
            this.Minusas = new System.Windows.Forms.Button();
            this.Daugyba = new System.Windows.Forms.Button();
            this.Dalyba = new System.Windows.Forms.Button();
            this.Lygu = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.OrangeRed;
            this.button1.Location = new System.Drawing.Point(12, 122);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(49, 39);
            this.button1.TabIndex = 0;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = false;
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.OrangeRed;
            this.button2.Location = new System.Drawing.Point(82, 122);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(49, 39);
            this.button2.TabIndex = 1;
            this.button2.Text = "button2";
            this.button2.UseVisualStyleBackColor = false;
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.OrangeRed;
            this.button3.Location = new System.Drawing.Point(152, 122);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(49, 39);
            this.button3.TabIndex = 2;
            this.button3.Text = "button3";
            this.button3.UseVisualStyleBackColor = false;
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.OrangeRed;
            this.button4.Location = new System.Drawing.Point(12, 167);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(49, 39);
            this.button4.TabIndex = 3;
            this.button4.Text = "button4";
            this.button4.UseVisualStyleBackColor = false;
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.OrangeRed;
            this.button5.Location = new System.Drawing.Point(82, 167);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(49, 39);
            this.button5.TabIndex = 4;
            this.button5.Text = "button5";
            this.button5.UseVisualStyleBackColor = false;
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.Color.OrangeRed;
            this.button6.Location = new System.Drawing.Point(152, 167);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(49, 39);
            this.button6.TabIndex = 5;
            this.button6.Text = "button6";
            this.button6.UseVisualStyleBackColor = false;
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.Color.OrangeRed;
            this.button7.Location = new System.Drawing.Point(12, 212);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(49, 39);
            this.button7.TabIndex = 6;
            this.button7.Text = "button7";
            this.button7.UseVisualStyleBackColor = false;
            // 
            // button8
            // 
            this.button8.BackColor = System.Drawing.Color.OrangeRed;
            this.button8.Location = new System.Drawing.Point(82, 212);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(49, 39);
            this.button8.TabIndex = 7;
            this.button8.Text = "button8";
            this.button8.UseVisualStyleBackColor = false;
            // 
            // button9
            // 
            this.button9.BackColor = System.Drawing.Color.OrangeRed;
            this.button9.Location = new System.Drawing.Point(152, 212);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(49, 39);
            this.button9.TabIndex = 8;
            this.button9.Text = "button9";
            this.button9.UseVisualStyleBackColor = false;
            // 
            // Pliusas
            // 
            this.Pliusas.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Pliusas.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Pliusas.Image = ((System.Drawing.Image)(resources.GetObject("Pliusas.Image")));
            this.Pliusas.Location = new System.Drawing.Point(221, 122);
            this.Pliusas.Name = "Pliusas";
            this.Pliusas.Size = new System.Drawing.Size(79, 39);
            this.Pliusas.TabIndex = 9;
            this.Pliusas.Text = "+";
            this.Pliusas.UseVisualStyleBackColor = false;
            // 
            // Minusas
            // 
            this.Minusas.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Minusas.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Minusas.Image = ((System.Drawing.Image)(resources.GetObject("Minusas.Image")));
            this.Minusas.Location = new System.Drawing.Point(221, 167);
            this.Minusas.Name = "Minusas";
            this.Minusas.Size = new System.Drawing.Size(79, 39);
            this.Minusas.TabIndex = 10;
            this.Minusas.Text = "-";
            this.Minusas.UseVisualStyleBackColor = false;
            // 
            // Daugyba
            // 
            this.Daugyba.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Daugyba.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Daugyba.Image = ((System.Drawing.Image)(resources.GetObject("Daugyba.Image")));
            this.Daugyba.Location = new System.Drawing.Point(221, 212);
            this.Daugyba.Name = "Daugyba";
            this.Daugyba.Size = new System.Drawing.Size(79, 39);
            this.Daugyba.TabIndex = 11;
            this.Daugyba.Text = "*";
            this.Daugyba.UseVisualStyleBackColor = false;
            // 
            // Dalyba
            // 
            this.Dalyba.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Dalyba.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Dalyba.Image = ((System.Drawing.Image)(resources.GetObject("Dalyba.Image")));
            this.Dalyba.Location = new System.Drawing.Point(221, 257);
            this.Dalyba.Name = "Dalyba";
            this.Dalyba.Size = new System.Drawing.Size(79, 39);
            this.Dalyba.TabIndex = 12;
            this.Dalyba.Text = "/";
            this.Dalyba.UseVisualStyleBackColor = false;
            // 
            // Lygu
            // 
            this.Lygu.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Lygu.Image = ((System.Drawing.Image)(resources.GetObject("Lygu.Image")));
            this.Lygu.Location = new System.Drawing.Point(12, 257);
            this.Lygu.Name = "Lygu";
            this.Lygu.Size = new System.Drawing.Size(189, 39);
            this.Lygu.TabIndex = 13;
            this.Lygu.Text = "Lygu";
            this.Lygu.UseVisualStyleBackColor = true;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(13, 21);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(48, 20);
            this.textBox1.TabIndex = 14;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(67, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 15;
            this.label1.Text = "label1";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(108, 21);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(54, 20);
            this.textBox2.TabIndex = 16;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(169, 27);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(13, 13);
            this.label2.TabIndex = 17;
            this.label2.Text = "=";
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(189, 20);
            this.textBox3.Name = "textBox3";
            this.textBox3.ReadOnly = true;
            this.textBox3.Size = new System.Drawing.Size(100, 20);
            this.textBox3.TabIndex = 18;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(313, 304);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.Lygu);
            this.Controls.Add(this.Dalyba);
            this.Controls.Add(this.Daugyba);
            this.Controls.Add(this.Minusas);
            this.Controls.Add(this.Pliusas);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button Pliusas;
        private System.Windows.Forms.Button Minusas;
        private System.Windows.Forms.Button Daugyba;
        private System.Windows.Forms.Button Dalyba;
        private System.Windows.Forms.Button Lygu;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox3;
    }
}

